'use strict';

const express = require('express');
const Converter = require('csvtojson').Converter;

const csvFilePath = 'data/data.csv';
const app = express();
const converter = new Converter();
const PORT = 8000;

let dataJSON = {};

converter.fromFile(csvFilePath, (err, result) => {
  dataJSON = result;
});


// End Points

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  res.json(dataJSON);
});

app.listen(PORT);

console.log('API running in the port:', PORT);
