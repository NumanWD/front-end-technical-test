# Front-end Technical Test

The information about the application could be found in [docs](doc/README.md) folder.


## Set up

I am using Yarn as package manager. But you could use npm.

### API

To get the dependencies, we have to execute Yarn or (npm)

```
yarn
```

### Front

To get the dependencies, we have to execute Yarn or (npm)

```
yarn
```


## Running

First we need to start the API. We go to the api folder `api` and start the process.

```
yarn run start
```

After that we start the webserver of our application. In the folder front `front`.

```
yarn run start
```


## Testing

To run test in the React application, we are using jest. Go to the front folder `front` and run

```
yarn run test
```
