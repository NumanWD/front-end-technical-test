import React from 'react';
import { shallow, mount } from 'enzyme';
import expect from 'expect';
import Graph from '../js/Graph.jsx';
import { Bubble } from 'react-chartjs-2';

describe('<Graph />', () => {

  it('Should renders one Bubble graphic component', () => {
    const component = shallow(<Graph />);
    expect(component.find(Bubble).length).toBe(1);
  });

});
