import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Header from '../js/Header.jsx';

describe('<Header />', () => {

  it('Header render the title (Snapshot)', () => {
    const component = renderer.create(
      <Header title="New Title"/>
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Header renders the title', () => {
    const component = shallow(
      <Header title="Something"/>
    );
    expect(component.find('h1').text()).toEqual('Something');
  });

});
