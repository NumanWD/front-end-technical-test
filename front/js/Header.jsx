import React from 'react';

const { string } = React.PropTypes;

const Header = (props) => (
  <header>
    <h1>{props.title}</h1>
  </header>
);

Header.propTypes = {
  title: string.isRequired,
};

export default Header;
