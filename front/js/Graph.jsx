import React from 'react';
import Axios from 'axios';
import { Bubble } from 'react-chartjs-2';
import bubbleConfig from './config/bubbleConfig';


class Graph extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      coordinates: [],
    };
  }

  componentDidMount() {
    Axios.get('//localhost:8000')
      .then((response) => {
        this.setState({
          coordinates: response.data,
        });
      })
      .catch((error) => {
        window.console.error('axios error', error);
      });
  }

  render() {
    bubbleConfig.datasets[0].data = this.state.coordinates;

    return (
      <main>
        <Bubble data={bubbleConfig} />
      </main>
    );
  }
}

export default Graph;
