import React from 'react';
import { render } from 'react-dom';
import Graph from './Graph';
import Header from './Header';

const App = () => {
  return (
    <div>
      <Header title="Front-end Technical Test" />
      <Graph />
    </div>
  );
};

render(<App />, document.getElementById('app'));
