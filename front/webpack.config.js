const path = require('path');

module.exports = {
  context: __dirname,
  entry: './js/ClientApp',
  devtool: 'eval',
  output: {
    path: path.join(__dirname, '/public/js/'),
    filename: 'bundle.js',
    publicPath: '/public/',
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    historyApiFallback: true,
  },
  resolve: {
    extensions: ['.jsx', '.js'],
  },
  stats: {
    colors: true,
    reasons: true,
    chunks: true,
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.jsx$/,
        loader: 'eslint-loader',
        exclude: /node_modules/,
      },
      {
        include: path.resolve(__dirname, 'js'),
        test: /\.jsx$/,
        loader: 'babel-loader',
      },
    ],
  },
};
