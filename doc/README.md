# Nutmeg Front-end Technical Test

You are required to write an application using JavaScript, that will render a Scatter Plot to display all model data. The data has been provided as a CSV file, that should be served via an endpoint., the preferred method of delivery would be done using NodeJS, however if you would like to use another language for this part, then that would be acceptable. The preferred method to render the front-end would be delivered using ReactJS.


## Requirements
-	Deliver an endpoint that will provide the Scatter Plot data in JSON format
-	Render a page that will display a Scatter Plot, with all the points that are provided in the CSV file
-	You can use a third-party component of your choice to draw the Scatter Plot
-	Demonstrate a way to manage state in the ReactJS application
-	Provide some tests


## Bonus Points
-	Include some build steps to compile the application into a directory
-	Optimise the API request by using caching, etc…
-	Use some mechanism to show code quality and standards analysis output


The task should not take longer than 2 hours to complete. You will find the model data in the file provided as data.csv.

